<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Pusher\Pusher;
use Pusher\PusherException;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function authenticate(Request $request)
    {
        $socketId = $request->socket_id;
        $channelName=$request->channel_name;
        try {
            $pusher = new Pusher('a98dc9b6ff7c0653405f', '8710a991ce70e6a7954b', '918512', [
                'cluster' => "ap2",
                'forceTLS' => true,
            ]);
        } catch (PusherException $e) {
            echo "pusher error";
        }
        $presence_data = ['name' => auth()->user()->name];
        try {
            $key = $pusher->presence_auth($channelName, $socketId, auth()->id(), $presence_data);
        } catch (PusherException $e) {
            echo "channel connection error";
        }

        return response($key);
    }
}
